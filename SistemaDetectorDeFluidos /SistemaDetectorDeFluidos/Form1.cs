﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Vision.Motion;
using System.Windows.Forms;
using AForge.Imaging;
using AForge.Imaging.Filters;
using System.Drawing.Imaging;
using System.IO;

namespace SistemaDetectorDeFluidos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }     
        public FilterInfoCollection Dispositivos;
        public VideoCaptureDevice FuenteVideo;
        bool iniciar = false;
        Int32 R=0, G=0, B=0;
        protected override void OnLoad(EventArgs e)
        {
            InicioCamaras();
            btnSalir.Enabled = false;                
        }
        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (FuenteVideo.IsRunning || FuenteVideo.IsRunning)
            {
                FuenteVideo.Stop();
            }
            Application.Exit();
        }
        private void InicioCamaras()
        {
            Dispositivos = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            foreach (FilterInfo Camaras in Dispositivos)
            {

                cbCamaras.Items.Add(Camaras.Name);

            }
            cbCamaras.SelectedIndex = 0;
        }    
        private void bntCamaras_Click(object sender, EventArgs e)
        {        
            FuenteVideo = new VideoCaptureDevice(Dispositivos[cbCamaras.SelectedIndex].MonikerString);
            FuenteVideo.NewFrame += new NewFrameEventHandler(VideoFinal_Frame);
            FuenteVideo.Start();
            btnSalir.Enabled = true;
        }
        private void VideoFinal_Frame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap image = (Bitmap)eventArgs.Frame.Clone();
            Bitmap image1 = (Bitmap)eventArgs.Frame.Clone();
            pbImagen.Image = image;
            if (rbLocalizarObjeto.Checked)
            {
                EuclideanColorFiltering filtro1 = new EuclideanColorFiltering();             
                filtro1.CenterColor = new RGB(Color.FromArgb(R,G,B));
                filtro1.Radius = 85;
                filtro1.ApplyInPlace(image1);
                Localizacion(image1);                           
            }
            if (rbObjetos.Checked)
            {
                EuclideanColorFiltering filtro2 = new EuclideanColorFiltering();
                filtro2.CenterColor = new RGB(Color.FromArgb(R, G, B));
                filtro2.Radius = 85;
                filtro2.ApplyInPlace(image1);
                LocalizarObjetos(image1);
            }           
        }
        private void tbRed_Scroll(object sender, EventArgs e)
        {
            R = tbRed.Value;
        }
        private void tbGreen_Scroll(object sender, EventArgs e)
        {
            G = tbGreen.Value;
        }
        private void tbBlue_Scroll(object sender, EventArgs e)
        {
            B = tbBlue.Value;
        }
        private void rbtnBlanvoyNegro_CheckedChanged(object sender, EventArgs e)
        {         
                if (FuenteVideo.IsRunning)
                {
                    iniciar = true;
                }
                else
                {
                    iniciar = false;
                }
            } 
        private void pbImagen_Paint(object sender, PaintEventArgs e)
        {
            if (iniciar == true)
            {
                Bitmap img11 = new Bitmap(pbUltravioleta.Image);  
            }
        }
        private void Localizacion(Bitmap imagen)
        {
            BlobCounter muestrapunto = new BlobCounter();
            muestrapunto.MinWidth = 5;
            muestrapunto.MinHeight = 5;
            muestrapunto.FilterBlobs = true;
            muestrapunto.ObjectsOrder = ObjectsOrder.Size;
            BitmapData DatosDeObjetos = imagen.LockBits
            (new Rectangle(0, 0, imagen.Width, imagen.Height), ImageLockMode.ReadOnly, imagen.PixelFormat);  
            imagen.UnlockBits(DatosDeObjetos);
            muestrapunto.ProcessImage(imagen);
            Rectangle[] rects = muestrapunto.GetObjectsRectangles();      
            Blob[] blobs = muestrapunto.GetObjectsInformation();
            pbUltravioleta.Image = imagen;
            foreach (Rectangle recs in rects)
                {
                    if (rects.Length > 0)
                    {
                        Rectangle objectRect = rects[0];                     
                        Graphics g = pbImagen.CreateGraphics();
                        using (Pen pen = new Pen(Color.FromArgb(R,G,B), 2))
                        {
                            g.DrawRectangle(pen, objectRect);
                        }                     
                        int objectX = objectRect.X + (objectRect.Width / 2);
                        int objectY = objectRect.Y + (objectRect.Height / 2);
                        g.Dispose();                           
                        }
                    }
           
            }     
        private void LocalizarObjetos(Bitmap imagen2)
        {
            BlobCounter muestrapunto = new BlobCounter();
            muestrapunto.MinWidth = 5;
            muestrapunto.MinHeight = 5;
            muestrapunto.FilterBlobs = true;
            muestrapunto.ObjectsOrder = ObjectsOrder.Size;
            BitmapData DatosDeObjetos = imagen2.LockBits(new Rectangle
            (0, 0, imagen2.Width, imagen2.Height), ImageLockMode.ReadOnly, imagen2.PixelFormat);
            imagen2.UnlockBits(DatosDeObjetos);
            muestrapunto.ProcessImage(imagen2);
            Rectangle[] rects = muestrapunto.GetObjectsRectangles();
            Blob[] blobs = muestrapunto.GetObjectsInformation();
            pbUltravioleta.Image = imagen2;
                for (int i = 0; rects.Length > i; i++)
                {
                    Rectangle objectRect = rects[i];
                    Graphics graficar = pbImagen.CreateGraphics();
                    using (Pen pluma = new Pen(Color.FromArgb(R,G,B), 2))
                    {
                        graficar.DrawRectangle(pluma, objectRect);
                        //graficar.DrawString((i + 1).ToString(), new Font("Arial", 9), Brushes.Blue, objectRect);
                        //PARS PONER NUMEROS EN LOS TRIANGULOS
                    }
                    int X = objectRect.X + (objectRect.Width / 2);
                    int Y = objectRect.Y + (objectRect.Height / 2);
                    graficar.Dispose();
                }
           
        }
        private void pictureBox4_Click(object sender, EventArgs e)
        {       
                if (string.IsNullOrEmpty(txtCaso.Text))
                {
                    MessageBox.Show("Ingresa el nombre del caso", "Error",
                    MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                }
                else
                {
                    GuardarImagen();
                    MessageBox.Show("Guardado");
                }              
        }
        private void GuardarImagen()
        {
            string documento = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            string ruta = Path.Combine(documento, txtCaso.Text + ".jpg");
            pbUltravioleta.Image.Save(ruta, ImageFormat.Jpeg);
        } 
        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Caso guardado", "Forense");
            txtCaso.Enabled = false;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Start();
            label7.Text = R.ToString();
            label8.Text = G.ToString();
            label9.Text = B.ToString();
        }
        }
}